const todoService = require("../services/todo_service");
const logger = require('../helper/logger').logger
class TodoListController {
    constructor() {
        logger.info("Todo Controller init")
    }

    create(todoReq, res) {
            return todoService.createTodo(todoReq.body).then((docs) => {
                return docs
            }).catch((error) => {
                return error;
            });
    }

    async getAll(req, res) {
        return todoService.getAll().then((docs) => {
            return docs;
        }).catch(error => {
            return error;
        })
    }

    async updateTodoById(req, res) {
        return todoService.updateTodoById(req.body).then(data => {
            return data;
        }).catch((error) => {
            return error
        })
    }

    async deleteById(req, res) {
        return todoService.deleteById(req).then(data => {
            return data;
        }).catch((error) => {
            return error;
        })
    }
}

module.exports = new TodoListController()