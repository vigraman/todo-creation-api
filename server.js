const db = require("./model");
const app = require("express")();
const bodyParser = require('body-parser');
const logger = require('./helper/logger').logger;
const cors = require('cors');
const port = require("./config").port

class App {
    constructor() {
        this.init();
    }

    init() {
        this.initDB();
        this.initHttpServer();
        this.initControllers();
        this.initRoutes()
    }

    initDB() {
        db.connect();
    }

    initHttpServer() {
        app.use(bodyParser.json());
        app.use(cors());
        app.use(
            cors({
              orgin: [
                'http://localhost:3000/'
              ]
            })
        );
        app.listen(port, () => {
            logger.info("Listening in port::::", port)
        })
    }

    initControllers() {
        this.todoListController = require("./controllers/todo.controller");
    }

    initRoutes() {
        const todoRoutes = require('./routes/todo_routes.js')(this.todoListController);
        app.use('/api/todo', todoRoutes.getRouter());
    }
}

module.exports = new App();