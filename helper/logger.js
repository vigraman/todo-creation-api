const env = process.env.NODE_ENV || 'development'
module.exports.logger = require('logger').createLogger(`./${[env]}.log`) // logs to a file