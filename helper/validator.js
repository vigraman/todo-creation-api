const JoiBase = require('@hapi/joi');
const JoiDate = require('@hapi/joi-date');

const CONSTANT = require('../helper/constant');

const Joi = JoiBase.extend(JoiDate);

const schema = Joi.object().keys({
    action: Joi.string().required(),
    date: Joi.date().format(CONSTANT.DATE_TIME_FORMAT).required()
});

module.exports = async(req, res, next) => {
    const {error} = schema.validate(req.body);
    if(error) {
        if(error.details[0].path[0] === "date") {
            const resp = {code: 400, message:  "DATE_INVALID"}
            return res.status(200).json(resp);
        } else if(error.details[0].path[0] === "action") {
            const resp = {code: 400, message:  "ACTION_REQUIRED"}
            return res.status(200).json(resp);
        }
    }
    next();
}
