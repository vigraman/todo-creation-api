const moment = require("moment");

const dateTimeFormatChange = (date, expression) => {
    return moment(new Date(date)).format(expression)
}

module.exports.dateTimeFormatChange = dateTimeFormatChange;