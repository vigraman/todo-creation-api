const todoModel = require("../model/todo.model");
const utils = require("../helper/utils")
const CONSTANT = require("../helper/constant")
const logger = require('../helper/logger').logger

class TodoService {
    createTodo(todo) {
        return new Promise((resolve, reject) => {
            todoModel.create(todo).then(async (docs) => {
                const resp = {...docs._doc}
                resp.date = await utils.dateTimeFormatChange(resp.date, CONSTANT.DATE_TIME_FORMAT);
                resp.message = "CREATED"
                resolve(resp)
            }).catch((error) => {
                if(error) {
                    logger.error("Error in Todo Insertion", error)
                    reject({ code: 402, message: 'ALL_FIELDS_REQUIRED' });
                }

            });;
        })
    }

    getAll() {
        return new Promise(async (resolve, reject) => {
            todoModel.find().exec((err, docs) => {
                if(!err) {
                    resolve(docs);
                } else {
                    reject(err);
                }
            })
        })
    }


    updateTodoById(todo) {
        return new Promise(async (resolve, reject) => {
            todoModel.findOneAndUpdate({_id: todo._id}, { $set: {action: todo.action, date: todo.date}}, {new: false}, (error, doc) => {
                if(error) {
                    reject({ code: 408, message: 'SOMETHING_WRONG' });
                    return;
                  }
                  if (!doc) {
                    reject({ code: 404, message: 'NOT_FOUND' });
                    return;
                  } else {
                    resolve({ code: 200, message: 'UPDATED_SUCCESSFULLY' });
                  }
            })
        })
    }

    deleteById(req) {
        return new Promise(async (resolve, reject) => {
            todoModel.find({_id: req.params.id}).deleteOne().exec((error, docs) => {
                if (error) {
                    reject({ code: 408, message: 'SOMETHING_WRONG' });
                    return;
                  }
                  if (docs.deletedCount === 0) {
                      reject({ code: 404, message: 'NOT_FOUND' });
                  }
                  else {
                      resolve({ code: 200, message: 'DELETED_SUCCESSFULLY' });
                  } 
            })
        })
    }
}

module.exports = new TodoService();