const mongoose = require("mongoose");

var TodoListSchema = new mongoose.Schema({
    action: {
        type: String,
        required: "Required"
    },
    date: {
        type: Date,
        required: "Required"
    },
}, {timestamps: true})


module.exports = mongoose.model("lists", TodoListSchema);;

