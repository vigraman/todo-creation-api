const mongo = require("mongoose");
const config = require('../config/index');
const mongoUrl = `mongodb://${config.host}:${config.dbport}/${config.database}`;
const logger = require('../helper/logger').logger

connect = () => {
    mongo.connect(mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true}, (error) => {
        if(!error) {
            logger.info("Database Connection in the port::::", config.dbport)
        } else {
            logger.error("Error in Database connection::::", e)
        }
    })    
}

module.exports.connect = connect;