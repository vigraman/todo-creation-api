const router = require('express').Router();
const logger = require('../helper/logger').logger
const todoCreationValidator = require('../helper/validator');
class TodoRoutes {
    constructor(controller) {
        this.controller = controller;
        this.init();
        logger.info("Routes Initializing");
    }

    init() {
        router.use('/', async (req, res, next) => {
            next()
        });
        router.post("/create", todoCreationValidator, async (req, res) => {
            try {                
                const resp = await this.controller.create(req, res);
                res.send(resp);
            } catch(error) {
                logger.error("Error in todo creation::::", error);
            }
        });

        router.get("/get-all", async (req, res) => {
            try {
                const resp = await this.controller.getAll(req, res);
                res.send(resp);
            } catch(error) {
                logger.error("Error in fetch::::", error);
            }
        });

        router.put("/update-todo", todoCreationValidator, async (req, res) => {
            try {
                const resp = await this.controller.updateTodoById(req, res);
                res.send(resp);
            } catch(error) {
                logger.error("Error on Update::::", error)
            }
        })

        router.delete("/delete-todo/:id", async (req, res) => {
            try {
                const resp = await this.controller.deleteById(req, res);
                res.send(resp);
            } catch(error) {
                logger.error("Error in Deletion::::", error);
            }
        })
    }

    getRouter() {
        return router;
    }
}

module.exports = controller => new TodoRoutes(controller);